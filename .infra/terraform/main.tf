terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.80"
}

provider "yandex" {
  service_account_key_file = file(var.service)
  cloud_id                 = var.cloud
  folder_id                = var.folder
  zone                     = var.zone
}

provider "local"{
}
